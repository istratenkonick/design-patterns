<?php


namespace App\DesignPatterns\Adapter\Contracts;


interface ExcelCreatorNewContract
{
    /**
     * @param string $title
     * @return mixed
     */
    public function addColumn(string $title): self;

    /**
     * @param array $data
     * @return mixed
     */
    public function addRow(array $data): self;

    /**
     * @param int $number
     * @return mixed
     */
    public function removeColumn(int $number): self;

    /**
     * @param int $number
     * @return mixed
     */
    public function removeRow(int $number): self;

    /**
     * @return mixed
     */
    public function build();
}
