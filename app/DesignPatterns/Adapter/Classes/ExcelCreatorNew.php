<?php


namespace App\DesignPatterns\Adapter\Classes;


use App\DesignPatterns\Adapter\Contracts\ExcelCreatorNewContract;
use Barryvdh\Debugbar\Facade;

class ExcelCreatorNew implements ExcelCreatorNewContract
{
    /**
     * @var array
     */
    public array $columns = [];

    /**
     * @var array
     */
    public array $rows = [];

    /**
     * @param string $title
     * @return mixed
     */
    public function addColumn(string $title): ExcelCreatorNewContract
    {
        $this->columns[] = $title;

        return $this;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function addRow(array $data): ExcelCreatorNewContract
    {
        $this->rows[] = $data;

        return $this;
    }

    /**
     * @param int $number
     * @return mixed
     */
    public function removeColumn(int $number): ExcelCreatorNewContract
    {
        if(isset($this->columns[$number - 1])){
            unset($this->columns[$number - 1]);

            sort($this->columns);
        }
    }

    /**
     * @param int $number
     * @return mixed
     */
    public function removeRow(int $number): ExcelCreatorNewContract
    {
        if(isset($this->rows[$number - 1])){
            unset($this->rows[$number - 1]);

            sort($this->rows);
        }
    }

    /**
     * @return mixed
     */
    public function build()
    {
        Facade::debug(
            array_merge([$this->columns], $this->rows)
        );
    }
}
