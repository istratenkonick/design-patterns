<?php


namespace App\DesignPatterns\EventChannel\Channel;


use App\DesignPatterns\EventChannel\Subscriber\SubscriberContract;

interface EventChannelContract
{
    /**
     * @param string $topic
     * @param SubscriberContract $subscriber
     * @return void
     */
    public function subscribe(string $topic, SubscriberContract $subscriber): void;

    /**
     * @param string $topic
     * @param $data
     * @return void
     */
    public function publish(string $topic, $data): void;
}
