<?php


namespace App\DesignPatterns\Singleton\Traits;


trait SingletonTrait
{
    /**
     * @var $this
     */
    public static self $instance;

    /**
     * Forbid for create new instance.
     */
    private function __construct()
    {
        //
    }

    /**
     * Forbid for clone new instance
     */
    private function __clone()
    {
        //
    }

    /**
     * Forbid for serialize new instance
     */
    private function __sleep()
    {
        //
    }

    /**
     * Forbid for deserialize new instance
     */
    private function __wakeup()
    {
        //
    }

    /**
     * Return singleton instance
     *
     * @return static
     */
    public static function getInstance(): self
    {
        return isset(self::$instance) ? self::$instance : self::$instance = new static();
    }
}
