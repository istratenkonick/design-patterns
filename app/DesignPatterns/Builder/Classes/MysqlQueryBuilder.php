<?php


namespace App\DesignPatterns\Builder\Classes;


use App\DesignPatterns\Builder\Contracts\SqlQueryBuilder;
use Exception;

class MysqlQueryBuilder implements SqlQueryBuilder
{
    /**
     * @var string
     */
    protected string $table;

    /**
     * @var array
     */
    protected array $select;

    /**
     * @var array
     */
    protected array $where;

    /**
     * @var int
     */
    protected int $limit;

    /**
     * @return mixed
     */
    public static function query(): SqlQueryBuilder
    {
        return new static();
    }

    /**
     * @param string $table
     * @return $this
     */
    public function table(string $table): SqlQueryBuilder
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function select(array $columns): SqlQueryBuilder
    {
        $this->select = $columns;

        return $this;
    }

    /**
     * @param string $column
     * @param string $action
     * @param $value
     * @return $this
     */
    public function where(string $column, string $action, $value): SqlQueryBuilder
    {
        $this->where[] = [$column, $action, $value];

        return $this;
    }

    /**
     * @param int $amount
     * @return $this
     */
    public function limit(int $amount): SqlQueryBuilder
    {
        $this->limit = $amount;

        return $this;
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    public function get(): string
    {
        $query = $this->baseQuery('select');

        $query = sprintf(
            $query, isset($this->select) ? implode(', ', $this->select) : '*'
        );

        $query .= $this->conditionsQuery();

        $query .= $this->limitQuery();

        return $query;
    }

    /**
     * @param $type
     * @return string
     * @throws Exception
     */
    private function baseQuery($type): string
    {
        if(!isset($this->table)){
            throw new Exception('Table is not setted up');
        }

        switch ($type){
            case 'select':
                return 'SELECT %s FROM ' . $this->table;
        }
    }

    /**
     * @return null|string
     */
    private function conditionsQuery(): ?string
    {
        if(!isset($this->where)){
            return '';
        }

        $conditions_query = [];

        foreach ($this->where as $condition){
            [$column, $action, $value] = $condition;

            $conditions_query[] = $column . ' ' . $action . ' ' . $value;
        }

        return ' WHERE ' . implode(' AND ', $conditions_query);
    }

    /**
     * @return null|string
     */
    public function limitQuery(): ?string
    {
        if(!isset($this->limit)){
            return null;
        }

        return ' LIMIT ' . $this->limit;
    }
}
