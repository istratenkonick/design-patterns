<?php


namespace App\DesignPatterns\Builder\Models;


use App\DesignPatterns\Builder\Classes\MysqlQueryBuilder;

class Order extends MysqlQueryBuilder
{
    protected string $table = 'orders';
}
