<?php


namespace App\DesignPatterns;


use Illuminate\Support\Facades\Route;

class Helper
{
    /**
     * @return array
     */
    public static function mainDefinition(): array
    {
        return [
            'title' => 'Design Patterns (Шаблоны проектирования)',
            'description' => '
                <hr>
                Шпаргалка для шаблонов проектирования
                <br>
                <br>
                Реализации находятся по пути "app/DesignPatters" в одноимённых папках.
                <br>
                Все сообщения выполненные в контроллерах находятся в разделе "Messages" в DebugBar
                <br>
                <br>
                На индекс странице есть краткое описание каждого паттерна и ссылка на маршрут в контроллере с использованием реализаций каждого из шаблона проектирования.
                <br> 
                Но чтобы понять, придётся залазить в код и самому изучать, что и как.
                <hr>
            '
        ];
    }

    /**
     * @return array
     */
    public static function propertyContainerDefinition(): array
    {
        return [
            'title' => 'Property Container (Контейнер Свойтсв)',
            'description' => '
                <hr>
                Фундаментальный шаблон проектирование "Контейнер свойств"
                <br>
                <br>
                Паттерн позволяет динамически добавлять свойства классу.
                <br>
                <br>
                Это удобно, когда мы не знаем, какие свойства могут принаджелать классу
                <br>
                <br>
                Пример использования: ларавел модель или объект соединённый с базой данных. Мы не знаем какие поля есть в базе данных,
                <br>
                а даже если и знаем не удобно для каждого объъкта создавать свойства основываясь на полях в бд.
                <br>
                <br>
                Мы можем получать все данные из БД и динамически заносить их в контейнер свойств
                <hr>
                <b>СМ. app/DesignPatterns/PropertyContainer</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function delegationDefinition(): array
    {
        return [
            'title' => 'Delegation (Делегирование)',
            'description' => '
                <hr>
                Фундаментальный шаблон проектирования "Делегирование".
                <br>
                <br>
                Его идея заключается в том, что мы можем мы можешь создать поведение объекта, который будет выполнять какие-либо действия,
                <br>
                хотя на самом деле, этот объект будет делегировать задачу на другой объект/класс при выполнении этой задачи
                <br>
                <br>
                Примером может послужить ларавел класс Request и его возможность валидировать данные. На самом деле валидирует данные не класс Request,
                <br>
                а класс Validator, которому делегирует задачу класс Request.
                <hr>
                <b>СМ. app/DesignPatterns/Delegation</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function eventChannelDefinition(): array
    {
        return [
            'title' => 'Event Channel (Канал Событий)',
            'description' => '
                <hr>
                Фундаментальный шаблон проектирование "Канал Событий"
                <br>
                <br>
                Шаблон позволяет устаналивать непрямую связь между двумя объектами через посредника. Посредником является непосредственно сам канал связи, а два объекта это Publisher и Subscriber.
                <br>
                <br>
                Publisher создаёт определённое событие и привязывается к каналу событий.
                <br>
                <br>
                Subscriber подписывается на событие в канале событий.
                <br>
                <br>
                Publisher может инициировать событие и тогда, канал связи на который Publisher был подписан оповестит Subscriber(-ов) о событии.
                <br>
                <br>
                Есть Аналог данного паттерна "Observer". Отличие заключается в том, что в данном шаблоне есть посредник в виде "Event Channel", когда у "Observer" два объекта связаны и взаимодействуют напрямую
                <hr>
                <b>СМ. app/DesignPatterns/EventChannel</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function interfaceDefinition(): array
    {
        return [
            'title' => 'Interface (Интерфейс)',
            'description' => '
                <hr>
                Фундаментальный шаблон проектирования "Интерфейс"
                <br>
                <br>
                Не путать с такой сущностью, как "Интерфейс" в языках программирования.
                <br>
                <br>
                По сути, сами того не зная, мы постоянно встречаемся с этим паттерном.
                <br>
                <br>
                Его концепция заключается в том, что какой-то высокоуровневый класс предоставляет доступ к методам и свойствам,
                <br>
                при этом не вдаваясь в подробности того, как он реализует или получает тот или иной метод/свойство.
                <br>
                <br>
                В качестве примера можно привести уже упомянутый класс Request Laravel. Он предоставляет обширный доступ к методам и свойствам,
                <br>
                при этот реализует он их на более низких уровнях класса, но нам, при его использовании нет необходимости это знать.
                <br>
                <br>
                Так же стоит отметить, что некоторые другие шаблоны используют данный шаблон проектирования, такие как "Фасад", "Делегирование" и др.
                <hr>
                <b>СМ. app/DesignPatterns/Interface</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function singletonDefinition(): array
    {
        return [
            'title' => 'Singleton (Одиночка)',
            'description' => '
                <hr>
                Порождающий шаблон проектирования "Одиночка"
                <br>
                <br>
                Этот паттерн гарантирует существования только одного объекта класса, который можно получить из любого места программы.
                <br>
                <br>                                
                Насколько простой, настолько и противоречивый шаблон. В некоторой степени он нарушает Single responsibility принцип, а так же в PHP нельзя быть точно увереннм, что будет создан лишь один экземпляр класса, есть дыры благодаря которым их может быть больше.
                <br>
                <br>
                Для примера можно привести класс для работы с Логированием, Конфигами, Базой Данных.
                <br>
                <br>
                Следует помнить, что нужно минимизровать изменения состояний такого класса, так как неизвестно в какой части программы какое состояние изменялось. 
                <br>
                <br>
                <hr>
                <b>СМ. app/DesignPatterns/Singleton</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function builderDefinition(): array
    {
        return [
            'title' => 'Builder (Строитель)',
            'description' => '
                <hr>
                Порождающий шаблон проектирования "Строитель"
                <br>
                <br>
                Основой этого шаблона проектирования является создание объектов пошагово. Используя один и тот же интерфейс мы можем получать разные объекты этого класса в зависимости от необхдимости.
                <br>
                <br>
                Хорошим примером является SQL builder. Мы пошагово строим запрос в зависимости от того, что нам нужно.
                <br>
                <hr>
                <b>СМ. app/DesignPatterns/Builder</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function lazyLoadDefinition(): array
    {
        return [
            'title' => 'LazyLoad (Ленивая загрузка)',
            'description' => '
                <hr>
                Порождающий шаблон проектирования "Ленивая загрузка"
                <br>
                <br>
                Основная идея этого шаблона заключается в том, чтобы при инициализации объекта отложить инициализацию связанных объектов или выполнение каких-то трудоёмких задач или вычислений для экономии памяти.
                <br>
                <br>
                Примером можно привести обычное отношение, например один ко многим. Не всегда получая объект Blog нам нужно сразу инициализировать все его комментарии, возможно комментарии нам вообще не нужны в некоторых ситуациях. 
                Здесь нам на помощь приходит ленивая загрузка. Мы получим нужные вычесления или связанный объект только тогда, когда мы к ним обратимся
                <br>
                <br>
                Однако у этого шаблона есть минус. Если мы работаем с большим кол-вом объектов, то намного выгоднее сразу проинициализировать необходимые нам свойства или вычесления.
                <hr>
                <b>СМ. app/DesignPatterns/LazyLoad</b>
            '
        ];
    }


    /**
     * @return array
     */
    public static function strategyDefinition(): array
    {
        return [
            'title' => 'Strategy (Стратегия)',
            'description' => '
                <hr>
                Поведенческий шаблон проектирования "Стратегия"
                <br>
                <br>
                Основной идеей этого шаблона является создание отдельных классов для схожих по существу (взаимозаменяемых) алгоритмов.
                <br>
                <br>
                В одном и том же участкке кода может вызываться тот или иной класс стратегии для выполнения алгоритма в зависимости от условия.
                <br>
                <br>
                Хорошим примером можно привести реализацию системы оплаты разными способами.
                <br> 
                Системы оплаты будут стратегиями оплаты и в зависимости от приходящего запроса, мы будем выбирать нужную стратегию и у нас ничего не сломается, так как все стратегии используют одинаковый интерфейс.
                <hr>
                <b>СМ. app/DesignPatterns/Strategy</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function observerDefinition(): array
    {
        return [
            'title' => 'Observer (Наблюдатель)',
            'description' => '
                <hr>
                Фундаментальный шаблон проектирования "Наблюдатель".
                <br>
                <br>
                Этот шаблон проектирования схож с шаблоном проектирования EventChannel,разница заключается лишь в том, что два объекта тесно связанны друг с другом.
                <br>
                <br>
                Publisher знает от всех своих Subscriber и непосредственно сам их всех уведомляет о событиях
                <hr>
                <b>СМ. app/DesignPatterns/EventChannel</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function adapterDefinition(): array
    {
        return [
            'title' => 'Adapter (Адаптер)',
            'description' => '
                <hr>
                Стркутурный шаблон проектирования "Адаптер".
                <br>
                <br>
                Название данного шаблона проектирования говорит само за себя. Благодаря данному паттерну, имея две схожие реализации с абсолютно разными интерфейсами мы можем подменить старый класс на новый делегирую ответственность на класс Адаптера.
                <hr>
                <b>СМ. app/DesignPatterns/EventChannel</b>
            '
        ];
    }

    /**
     * @return array
     */
    public static function getSideBarLinks(): array
    {
        return [
            [
                'name' => 'Fundamental',
                'children' => [
                    [
                        'name' => 'Property Container',
                        'route' => route('property-container'),
                        'active' => Route::currentRouteName() == 'property-container'
                    ],
                    [
                        'name' => 'Delegation',
                        'route' => route('delegate'),
                        'active' => Route::currentRouteName() == 'delegate'
                    ],
                    [
                        'name' => 'Event Channel',
                        'route' => route('event-channel'),
                        'active' => Route::currentRouteName() == 'event-channel'
                    ],
                    [
                        'name' => 'Interface',
                        'route' => route('interface'),
                        'active' => Route::currentRouteName() == 'interface'
                    ]
                ]
            ],
            [
                'name' => 'Creational',
                'children' => [
                    [
                        'name' => 'Singleton',
                        'route' => route('singleton'),
                        'active' => Route::currentRouteName() == 'singleton'
                    ],
                    [
                        'name' => 'Builder',
                        'route' => route('builder'),
                        'active' => Route::currentRouteName() == 'builder'
                    ],
                    [
                        'name' => 'LazyLoad',
                        'route' => route('lazy-load'),
                        'active' => Route::currentRouteName() == 'lazy-load'
                    ]
                ]
            ],
            [
                'name' => 'Behaviour',
                'children' => [
                    [
                        'name' => 'Strategy',
                        'route' => route('strategy'),
                        'active' => Route::currentRouteName() == 'strategy'
                    ],
                    [
                        'name' => 'Observer',
                        'route' => route('observer'),
                        'active' => Route::currentRouteName() == 'observer'
                    ]
                ]
            ],
            [
                'name' => 'Structural',
                'children' => [
                    [
                        'name' => 'Adapter',
                        'route' => route('adapter'),
                        'active' => Route::currentRouteName() == 'adapter'
                    ]
                ]
            ]
    ];
    }
}
