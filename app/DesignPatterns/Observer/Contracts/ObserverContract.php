<?php


namespace App\DesignPatterns\Observer\Contracts;


interface ObserverContract
{
    /**
     * @param ObservedContract $item
     */
    public function created(ObservedContract $item): void;

    /**
     * @param ObservedContract $item
     */
    public function updated(ObservedContract $item): void;

    /**
     * @param ObservedContract $item
     */
    public function deleted(ObservedContract $item): void;
}
