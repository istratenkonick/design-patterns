<?php


namespace App\DesignPatterns\Observer;


use App\DesignPatterns\Observer\Contracts\ObservedContract;
use App\DesignPatterns\Observer\Observers\AdminObserver;
use App\DesignPatterns\Observer\Observers\UserObserver;
use App\DesignPatterns\PropertyContainer\AbstractPropertyContainer;
use Exception;

class User extends AbstractPropertyContainer implements ObservedContract
{
    use CanBeObserved;

    /**
     * @var int
     */
    public int $id;

    /**
     * User constructor.
     *
     * @param array $attributes
     * @throws Exception
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->registerObservers();
        $this->fireEvent('created');
    }

    /**
     * @param array $data
     * @return void
     */
    public function update(array $data): void
    {
        $this->fillAttributes($data);

        $this->fireEvent('updated');

        session()->put('saved_items_' . $this->id, $this);
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        $this->fireEvent('deleted');

        session()->pull('saved_items_' . $this->id);
    }

    /**
     * @throws Exception
     */
    public function registerObservers(){

    }
}
