<?php


namespace App\DesignPatterns\Delegation\Validator;


class ValidatorError
{
    /**
     * @var array
     */
    private array $errors = [];

    /**
     * @return array
     */
    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * Add error to an object
     */
    public function setError($key, $message): void
    {
        $this->errors[$key][] = $message;
    }

    /**
     * @return bool
     */
    public function empty(): bool
    {
        return empty($this->errors);
    }
}
