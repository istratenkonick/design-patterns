<?php


namespace App\DesignPatterns\Delegation\Validator;


use Exception;
use Illuminate\Support\Str;

class Validator implements ValidatorContract
{
    use CanValidateAttributes;

    /**
     * @var array
     */
    private array $data;

    /**
     * @var array
     */
    private array $rules;

    /**
     * @var array
     */
    private array $messages;

    /**
     * @var ValidatorError
     */
    private ValidatorError $errorsBag;

    /**
     * Validator constructor.
     *
     * @param array $data
     * @param array $rules
     * @param array $messages
     */
    public function __construct(
        array $data,
        array $rules,
        array $messages = null
    )
    {
        $this->errorsBag = app(ValidatorError::class);
        $this->data = $data;
        $this->rules = $rules;
        $this->messages = $messages;
    }

    /**
     * @param array $data
     * @param array $rules
     * @param array $messages
     *
     * @return ValidatorContract
     */
    public static function make(array $data, array $rules, array $messages = null): ValidatorContract
    {
        return new static($data, $rules, $messages);
    }

    /**
     * Start validating process
     *
     * @return mixed|void
     * @throws Exception
     */
    public function validate()
    {
        if(!$this->data || !$this->rules){
            throw new Exception('Data or Rules not set');
        }

        foreach($this->rules as $key => $rules){
            if(in_array('required', $rules)){
                $this->validateRule($key, $this->data, 'required');
            }

            unset($rules[array_search('required', $rules)]);

            if(array_key_exists($key, $this->data)){
                foreach($rules as $rule){
                    $this->validateRule($key, $this->data[$key], $rule);

                }
            }
        }

        return $this->errorsBag->empty() ? true : $this->errorsBag->errors();
    }

    /**
     * Validate specified rule
     *
     * @param $key
     * @param $value
     * @param $rule
     */
    protected function validateRule($key, $value, $rule): void
    {
        $method = 'validate' . Str::studly($rule) . 'Rule';

        if(!method_exists($this, $method)){
            throw new \BadMethodCallException(sprintf(
                '%s method not found in validation list', $method
            ));
        }

        $validation = $this->{$method}($key, $value);

        if(!$validation){
            $this->errorsBag->setError($key, $this->getValidationMessage($key, $rule));
        }
    }

    /**
     * Get message for error object
     *
     * @param string $key
     * @param string $rule
     * @return string
     */
    protected function getValidationMessage(string $key, string $rule): string
    {
        return $this->messages[$key][$rule] ?? $this->getDefaultMessage($key, $rule);
    }

    /**
     * Get message from default list of messages
     *
     * @param string $key
     * @param string $rule
     * @return string
     */
    protected function getDefaultMessage(string $key, string $rule): string
    {
        $rules = [
            'required' => '%s must be required',
            'integer' => '%s must be an integer',
            'string' => '%s must be a string',
            'email' => '%s must be an email format'
        ];

        return sprintf($rules[$rule], Str::studly($key)) ?? 'No validation error message';
    }
}
