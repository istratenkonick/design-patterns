<?php


namespace App\DesignPatterns\LazyLoad\Classes;


use App\DesignPatterns\PropertyContainer\AbstractPropertyContainer;

class DefaultModel extends AbstractPropertyContainer
{
    /**
     * @var array
     */
    protected array $relations = [];

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if(method_exists($this, $name)){
            return $this->{$name}();
        }

        return $this->getAttribute($name);
    }
}
