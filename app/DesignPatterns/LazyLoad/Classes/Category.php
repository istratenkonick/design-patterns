<?php


namespace App\DesignPatterns\LazyLoad\Classes;


class Category extends DefaultModel
{
    /**
     * @return array
     */
    public function books()
    {
        if(!isset($this->relations[__FUNCTION__])){
            return $this->relations[__FUNCTION__] = [
                new Book(['name' => 'Book 1']),
                new Book(['name' => 'Book 2']),
                new Book(['name' => 'Book 3']),
                new Book(['name' => 'Book 4']),
            ];
        }

        return $this->relations[__FUNCTION__];
    }
}
