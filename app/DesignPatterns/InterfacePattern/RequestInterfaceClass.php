<?php


namespace App\DesignPatterns\InterfacePattern;


use App\DesignPatterns\Delegation\AppRequest;

/**
 * Just example class which extends all from AppRequest which implements to Interface Pattern
 */
class RequestInterfaceClass extends AppRequest
{

}
