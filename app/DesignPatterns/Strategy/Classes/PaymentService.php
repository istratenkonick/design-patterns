<?php


namespace App\DesignPatterns\Strategy\Classes;


use App\DesignPatterns\Strategy\Contracts\PaymentStrategy;
use App\DesignPatterns\Strategy\Contracts\PaymentService as PaymentContract;

class PaymentService implements PaymentContract
{
    /**
     * @var PaymentStrategy
     */
    private PaymentStrategy $strategy;

    /**
     * PaymentService constructor.
     * @param PaymentStrategy $strategy
     */
    public function __construct(PaymentStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    /**
     * @param PaymentStrategy $strategy
     * @return self
     */
    public static function make(PaymentStrategy $strategy): self
    {
        return new self($strategy);
    }

    /**
     * @param array $order_data
     * @return mixed
     */
    public function charge(array $order_data)
    {
        return $this->strategy->charge($order_data);
    }

    /**
     * @param $order_data
     * @param $validation_data
     * @return bool
     */
    public function validate(array $order_data, array $validation_data): bool
    {
        return $this->strategy->validate($order_data, $validation_data);
    }
}
