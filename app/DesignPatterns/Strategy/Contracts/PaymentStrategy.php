<?php


namespace App\DesignPatterns\Strategy\Contracts;


interface PaymentStrategy
{
    /**
     * @param $order_data
     * @return mixed
     */
    public function charge($order_data);

    /**
     * @param $order_data
     * @param $validate_data
     * @return mixed
     */
    public function validate($order_data, $validate_data);
}
