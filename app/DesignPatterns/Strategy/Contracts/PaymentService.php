<?php


namespace App\DesignPatterns\Strategy\Contracts;


interface PaymentService
{
    /**
     * @param array $order_data
     * @return mixed
     */
    public function charge(array $order_data);

    /**
     * @param array $order_data
     * @param array $validation_data
     * @return mixed
     */
    public function validate(array $order_data, array $validation_data);
}
