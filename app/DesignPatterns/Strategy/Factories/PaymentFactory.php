<?php


namespace App\DesignPatterns\Strategy\Factories;


use App\DesignPatterns\Strategy\Classes\CreditCardPayment;
use App\DesignPatterns\Strategy\Classes\PayPalPayment;
use App\DesignPatterns\Strategy\Classes\StripePayment;
use App\DesignPatterns\Strategy\Contracts\PaymentStrategy;
use Exception;

class PaymentFactory
{
    /**
     * @param string $type
     * @return PaymentStrategy
     * @throws Exception
     */
    public static function make(string $type): PaymentStrategy
    {
        switch ($type){
            case 'paypal':
                return new PayPalPayment();
            case 'stripe':
                return new StripePayment();
            case 'cc':
                return new CreditCardPayment();
            default:
                throw new Exception('Invalid payment type');
        }
    }
}
