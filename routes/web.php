<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PatternController@main')->name('home');

Route::prefix('patterns')->group(function(){
    Route::prefix('fundamental')->group(function(){
        Route::get('/property-container', 'PatternController@propertyContainerAction')->name('property-container');
        Route::get('/delegate', 'PatternController@delegateAction')->name('delegate');
        Route::get('/event-channel', 'PatternController@eventChannel')->name('event-channel');
        Route::get('/interface', 'PatternController@interface')->name('interface');
    });

    Route::prefix('creation')->group(function(){
        Route::get('/singleton', 'PatternController@singleton')->name('singleton');
        Route::get('/builder', 'PatternController@builder')->name('builder');
        Route::get('/lazy-load', 'PatternController@lazyLoad')->name('lazy-load');
    });

    Route::prefix('behaviour')->group(function(){
        Route::get('/strategy', 'PatternController@strategy')->name('strategy');
        Route::get('/observer', 'PatternController@observer')->name('observer');
    });

    Route::prefix('structural')->group(function(){
        Route::get('/adapter', 'PatternController@adapter')->name('adapter');
    });
});

